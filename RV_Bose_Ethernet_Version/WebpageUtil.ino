//Look for the below string and you can process everything after this String

String ledParam = "led";
String randomizeParam = "rdm";

String rCodeParam = "rcde";    //rCode
String gCodeParam = "gcde";    //gCode
String bCodeParam = "bcde";    //bCode

String respAction = "retresp";
const int sucessCode = 200;

String peakValParam = "peakval";

//Number of characters to look for after any Parameter
int paramValLength = 1;
int paramValLengthThree = 3;

//Looks like after the below value - the line ends
const String stopProcessingString = "HTTP/1.1";

void renderServer(){
  //Serial.println("renderServer");
  
  String HTTP_req;
  
  EthernetClient client = server.available();

  //long startTime = millis();
  
  if (client) {
    //Serial.println("New client available");
    
    String tempBufferString; 
    
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    
    //Stop processing after you get a request with ?led (lookForConst)
    boolean stopProcessing = false;

    while (client.connected()) {
      
        if (client.available()) {
            char c = client.read();
            //Serial.write(c);
           
            //The below statement appends only the first 100 characters - After which it will not append to rStr.
            
            //Will use only the tempBufferString after going thru full request- or u can enable the stopProcessing flag.
            //Stop processing after you get a request with stopProcessingString && the string should have values after lookFor for processing.
            //If its favicon - stop processing request.
            if(tempBufferString.indexOf("favicon.ico") > 0 ||  tempBufferString.indexOf(stopProcessingString) > 0)
            {
                stopProcessing = true;
            }
          
             //One value after the stopProcessingString will be stored
            tempBufferString += c; 

            // if you've gotten to the end of the line (received a newline character) and the line is blank, the http request has ended,
            // Or if its stopProcessing equals true
            // Response can be sent.
          
            if ((c == '\n' && currentLineIsBlank) || stopProcessing) {
                
               client.println("HTTP/1.1 200 OK");
               client.println("Server: Arduino");
               client.println("content-type:application/json; charset=utf-8"); 
               client.println("Access-Control-Allow-Origin: *");
               client.println("Connection: close");
               client.println();
                
               //client.println("<!DOCTYPE HTML>");
               //client.println("<link rel=\"shortcut icon\" href=\"data:image/x-icon;,\" type=\"image/x-icon\">");
                
                if(tempBufferString.indexOf("?") >= 0)
                {         
                    //2 is always get - return the current values.
                    if(tempBufferString.indexOf("led=2") > 0) {
                        
                        
                        
                    }
                    else if(tempBufferString.indexOf(ledParam) > 0) 
                    {
                        String ledStatusStr = processString(tempBufferString, ledParam, paramValLength );
                        ledStatus = ledStatusStr.toInt();
                    }
                    else if( tempBufferString.indexOf(rCodeParam) > 0  || tempBufferString.indexOf(gCodeParam) > 0 ||
                                        tempBufferString.indexOf(bCodeParam) > 0 ) 
                    {
                         //Parse Color Codes
                         
                         /******** Red Color Code Processing - Start ********/
                         String tempCodeVariable = processString(tempBufferString, rCodeParam , paramValLengthThree);
                         
                         if(!isValidNumber(tempCodeVariable)) {
                              //Handle errors
                              tempCodeVariable = "0";
                         }
                         rColorCode = tempCodeVariable.toInt();
                         /** Red Color Code Processing - End **/
                         
                         /** Green Color Code Processing - Start **/
                         tempCodeVariable = processString(tempBufferString, gCodeParam , paramValLengthThree);
                         if(!isValidNumber(tempCodeVariable)) {
                              tempCodeVariable = "0";
                         }
                         gColorCode = tempCodeVariable.toInt();
                         /** Green Color Code Processing - End **/

                         /** Blue Color Code Processing - Start **/
                         tempCodeVariable = processString(tempBufferString, bCodeParam , paramValLengthThree);
                         if(!isValidNumber(tempCodeVariable) ) {
                              tempCodeVariable = "0";
                         }
                         bColorCode = tempCodeVariable.toInt();
                         /** Blue Color Code Processing - End **/

                         //ShutDown randomize when colors are sent.
                         randomize = 0;
                    }
                    else if(tempBufferString.indexOf(randomizeParam) > 0) {
                         String randomizeStr = processString(tempBufferString, randomizeParam, paramValLength);
                         randomize = randomizeStr.toInt();
                    }
                }
                else {
                    //Default returns the status-Code
                    
                        
                }
                
                client.print(getCurrentStateAsJSON());
                client.println();
                break; 
        }
        
        if (c == '\n') {
            // you're starting a new line
            currentLineIsBlank = true;
        }
        else if (c != '\r') {
            // you've gotten a character on the current line
            currentLineIsBlank = false;
        }
      }
    }
    
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    //clearing string for next read
    //rStr="";
    //Serial.print("Data Sent In ...");

    //Serial.println(millis() - startTime);
  }
}

String getCurrentStateAsJSON(){

      String response = "{";
      String tempJson = "";

      //Led
      tempJson = createJsonString (ledParam , String(ledStatus));
      response.concat(tempJson);

      //Red Color
      tempJson = createJsonString (rCodeParam , String(rColorCode));
      response.concat(",");
      response.concat(tempJson);

      //Green Color
      tempJson = createJsonString (gCodeParam , String(gColorCode));
      response.concat(",");
      response.concat(tempJson);

      //Blue Color
      tempJson = createJsonString (bCodeParam , String(bColorCode));
      response.concat(",");
      response.concat(tempJson);
      
      //Random Param
      tempJson = createJsonString (randomizeParam, String(randomize));
      response.concat(",");
      response.concat(tempJson);
  
      //Max Peak Difference
      tempJson = createJsonString (peakValParam , String(maximumPeakDifference));
      response.concat(",");
      response.concat(tempJson);

      response.concat("}");

      return response;
}


//isNum = isDigit(str.charAt(i)) || str.charAt(i) == '+' || str.charAt(i) == '.' || str.charAt(i) == '-';
//Check if its a number - any digits - no plus or minus sign
boolean isValidNumber(String str){
   boolean isNum = false;
   for(byte i=0; i<str.length(); i++)
   {
       //loop thru each digit to check if its digit - can handle more condition here.    
       isNum = isDigit(str.charAt(i)); 
       if(!isNum)
       {
           //The first occurence of a non-digit character - stop and return false.
           return false;
       }
   }
   return isNum;
}


const char doubleQuote = '"'; 
/**
 * Creates a json formatted String for the passed key and value.
 */
String createJsonString (String jsonKey , String jsonVal) {
    String tempVal = String(doubleQuote);
    tempVal.concat(jsonKey);
    tempVal.concat(doubleQuote);
    tempVal.concat(":");
    tempVal.concat(doubleQuote);
    tempVal.concat(jsonVal);
    tempVal.concat(doubleQuote);
    return tempVal;
}


/**
 * Method to process and returns value 
 * Input string is usually the url with param
 * Looks for valueLength
 * Looks for the string to look for.
 */
String processString(String inputStr, String lookForString, int valueLength){
     //Retain only parameters from ? to blank space " "
     
     lookForString.concat("=");
     
     //Calculating the start and end indexes
     int startIdx = inputStr.indexOf(lookForString) + lookForString.length();
     int endIdx = startIdx + valueLength;
     String val = inputStr.substring(startIdx, endIdx);

     //Serial.print("led Status is ");
     //Serial.println(val);
     
     return val;
}

/*Sample Request IMPORTANT*/
/*Arduino Initialized....
New client
GET /?led=1 HTTP/1.1
Host: 192.168.0.16
Connection: keep-alive
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*//*;q=0.8  --> // escape Character - its only one in request
DNT: 1
Accept-Encoding: gzip, deflate, sdch
Accept-Language: en-US,en;q=0.8,ms;q=0.6

Value Substringed ...1
Data Sent In ...571
New client
GET /favicon.ico HTTP/1.1
Host: 192.168.0.16
Connection: keep-alive
Pragma: no-cache
Cache-Control: no-cache
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36
Accept: *//*--> // escape Character - its only one in request
DNT: 1
Referer: http://192.168.0.16/?led=1
Accept-Encoding: gzip, deflate, sdch
Accept-Language: en-US,en;q=0.8,ms;q=0.6
*/


