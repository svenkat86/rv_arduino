boolean clockWise = true;
int i =0;
void CanonBall(byte red, byte green, byte blue, int EyeSize, int SpeedDelay, int ReturnDelay){

  for(int i = 0; i < NUM_LEDS; i++) {
    setColorToOneStrip(0,0,0, clockWise);
    setColorToOneStripForGivenPixel(i, red/10, green/10, blue/10, clockWise);
    for(int j = 1; j <= EyeSize; j++) {
      setColorToOneStripForGivenPixel(i+j, red, green, blue, clockWise); 
    }
    setColorToOneStripForGivenPixel(i+EyeSize+1, red/10, green/10, blue/10 , clockWise);
    showOneStrip(clockWise);
    delay(SpeedDelay);
  }
  
  delay(ReturnDelay*4);
   
  for(int i = NUM_LEDS ; i >= 0-(EyeSize+1) ; i--) {
    setColorToOneStrip(0,0,0, !clockWise);
    setColorToOneStripForGivenPixel(i, red/10, green/10, blue/10, !clockWise);
    for(int j = 1; j <= EyeSize; j++) {
        setColorToOneStripForGivenPixel(i+j, red, green, blue , !clockWise); 
    }
      setColorToOneStripForGivenPixel(i, red/10, green/10, blue/10 , !clockWise);
      showOneStrip(!clockWise);
      delay(SpeedDelay);
  }
  
  //After 4 times lets rotate it back
  if(i == 10){
    i=0;
    clockWise = !clockWise;
    delay(ReturnDelay*4);
  }
  else{
    i++;
  }
  
  delay(ReturnDelay*4);
  
  /*for(int i = 0; i < NUM_LEDS; i++) {
    setAllOne(0,0,0, false);
    setPixelOne(i, red/10, green/10, blue/10, false);
    for(int j = 1; j <= EyeSize; j++) {
      setPixelOne(i+j, red, green, blue, false); 
    }
    setPixelOne(i+EyeSize+1, red/10, green/10, blue/10 , false);
    showStripOne(false);
    delay(SpeedDelay);
  }

  for(int i = NUM_LEDS; i >= 0-(EyeSize+1) ; i--) {
    setAllOne(0,0,0, true);
    setPixelOne(i, red/10, green/10, blue/10, true);
    for(int j = 1; j <= EyeSize; j++) {
      setPixelOne(i+j, red, green, blue , true); 
    }
    setPixelOne(i, red/10, green/10, blue/10, true);
    showStripOne(true);
    delay(SpeedDelay);
  }
  
  delay(ReturnDelay);*/
}
