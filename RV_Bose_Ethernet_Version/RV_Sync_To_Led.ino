void syncLedToMusic(){
        
        unsigned long startMillis= millis();  // Start of sample window
        unsigned int peakToPeak = 0;   // peak-to-peak level

        unsigned int signalMax = 0;
        unsigned int signalMin = 100;
        unsigned int sampleAvg = 0;
        
         // collect data for 50 mS
        while ((millis() - startMillis) < sampleWindow)
        { 
              int sampleL = analogRead(l_audio_Input);
              //int sampleR = analogRead(r_audio_Input);
                
              sampleAvg = sampleL; //(sampleL+sampleL)/2;
              
              if (sampleAvg < 1024)  // toss out spurious readings
              {
                 if (sampleAvg > signalMax)
                 {
                    signalMax = sampleAvg;  // save just the max levels
                 }
                 else if (sampleAvg < signalMin)
                 {
                    signalMin = sampleAvg;  // save just the min levels
                 }
              }
         }
                
       peakToPeak = signalMax - signalMin;
      
       int led = map(peakToPeak, 0, maximumPeakDifference, 0, NUM_LEDS ) - 1 ;

       //Turn on LEDS from bottom to up
       for(int i=0 ; i <= led; i++)
       {
           int color = map(i, 0, NUM_LEDS , 0, 255);
           stripL.setPixelColor(i, Wheel(color)); 
           stripR.setPixelColor(i, Wheel(color)); 
       }

       //Turning off the rest of LEDS from the top to Bottom.
       for(int i = NUM_LEDS ; i > led; i--)
       {
           stripL.setPixelColor(i, 0); 
           stripR.setPixelColor(i, 0);  
       }
       
       stripL.show();
       stripR.show();
       
}




uint32_t Wheel(byte WheelPos) {
    WheelPos = 255 - WheelPos;
    if(WheelPos < 85) {
        return stripL.Color(255 - WheelPos * 3, 0, WheelPos * 3);
    } 
    else if(WheelPos < 170) {
        WheelPos -= 85;
        return stripL.Color(0, WheelPos * 3, 255 - WheelPos * 3);
    } 
    else {
        WheelPos -= 170;
        return stripL.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
    }
}
