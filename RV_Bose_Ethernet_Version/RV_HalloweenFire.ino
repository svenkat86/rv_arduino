struct RGB {
  byte r;
  byte g;
  byte b;
};

//  The flame color array (the first is the default):
RGB flameColors[] = {
  { 226, 121, 35},  // Orange flame
  { 158, 8, 148},   // Purple flame 
  { 74, 150, 12},   // Green flame
  { 226, 15, 30}    // Red flame
};

//  Tracks the current color
int currentColorIndex = 0;


void HalloweeenFire(byte red, byte green, byte blue) {
    
    RGB currentColor = {red, green, blue};

      //  Flicker, based on our initial RGB values
      for(int i=0; i< NUM_LEDS ; i++) {
        int flicker = random(0,55);
        int r1 = currentColor.r - flicker;
        int g1 = currentColor.g - flicker;
        int b1 = currentColor.b - flicker;
        if(g1<0) g1=0;
        if(r1<0) r1=0;
        if(b1<0) b1=0;
        setPixel(i,r1,g1, b1);
      }

      showStrip();

      //  Adjust the delay here, if you'd like.  Right now, it randomizes the 
      //  color switch delay to give a sense of realism
      delay(random(10,113));
      //delay(random(0,1130));
    
}



