#include <Adafruit_NeoPixel.h>

//Input from aux - Left and Right Channel
const int l_audio_Input = A3;
const int r_audio_Input = A5;

//Controlling the LED Strips
#define LedPinL 5
#define LedPinR 6

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
const int NUM_LEDS = 28;

Adafruit_NeoPixel stripL = Adafruit_NeoPixel(NUM_LEDS, LedPinL, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel stripR = Adafruit_NeoPixel(NUM_LEDS, LedPinR, NEO_GRB + NEO_KHZ800);

//Attribute to store LED Status
volatile int ledStatus = 0;

int randomize = 0;

int rColorCode = random(0, 255);
int gColorCode = random(0, 255);
int bColorCode = random(0, 255);


const int sampleWindow = 10;  // Sample window width in mS (50 mS = 20Hz) - used in RV Sync class -from 10 changed to 20
int maximumPeakDifference = 110; 

//Setup function called..
void setup() {
    //Serial.begin(9600);
    //Serial.println("Arduino Initialized....");

    //Initialize Incoming Mic Input Pins
    pinMode(l_audio_Input, INPUT);
    pinMode(r_audio_Input, INPUT);

    //Initialization Of Ethernet Shield
    initializeEthernet();

    stripL.begin();
    stripL.show();

    stripR.begin();
    stripR.show();
    
}

void loop() {
      //Serial.print("Loops start....");
      //Serial.println(ledStatus);
    
      renderServer();

      //use Random color - Moved to top - so u can reset to zero wherever required.
      if(randomize == 1){
          rColorCode = random(0, 255);
          gColorCode = random(0, 255);
          bColorCode = random(0, 255);
      }
      

      if(ledStatus == 0){
           turnOffLedSystem();
      }
      else if(ledStatus == 2){
           //Do nothing - it will not go inside this block technically
      
      }
      else if(ledStatus == 1){
           HalloweeenFire(rColorCode , gColorCode , bColorCode);
      }
      else if(ledStatus == 3){
          //lets leave minimum value
          if(maximumPeakDifference > 30){
             maximumPeakDifference = maximumPeakDifference - 10;
          }
          //Reduce 5 and set back light to 4
          ledStatus = 4;
      }
      else if(ledStatus == 4){
          for(int i=0 ; i < 2 ; i++){
               syncLedToMusic();     
          }
      }
      else if(ledStatus == 5){
           maximumPeakDifference = maximumPeakDifference + 10;
            //Add 5 and set back light to 4
           ledStatus = 4;  
      }
      else if(ledStatus == 6){
            Fire(55,120,15);  
      }
      else if(ledStatus == 7){
            RunningLights(rColorCode , gColorCode , bColorCode, 50);
      }
      else if(ledStatus == 8){
            BouncingBalls(rColorCode, gColorCode, bColorCode , 2);
      }
      else if(ledStatus == 9){
           CanonBall(rColorCode, gColorCode, bColorCode , 3  , 15 , 50);
      }
      else{
           //can be off
      }

             
}
















