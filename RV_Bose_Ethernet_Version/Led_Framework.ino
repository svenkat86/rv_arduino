void showStrip() {
   #ifdef ADAFRUIT_NEOPIXEL_H 
     // NeoPixel
     stripL.show();
     stripR.show();
   #endif
}

void setBrighness(int b) {
   stripL.setBrightness(b);
   stripR.setBrightness(b);
}

/**
 * Sets Pixel to both strips
 */
void setPixel(int Pixel, byte red, byte green, byte blue) {
   #ifdef ADAFRUIT_NEOPIXEL_H 
     // NeoPixel
     stripL.setPixelColor(Pixel, stripL.Color(red, green, blue));
     stripR.setPixelColor(Pixel, stripR.Color(red, green, blue));
   #endif
}

/**
 * Sets to both Strips
 */
void setAll(byte red, byte green, byte blue) {
  for(int i = 0; i < NUM_LEDS; i++ ) {
      setPixel(i, red, green, blue); 
  }
  showStrip();
}


void turnOffLedSystem(){
    setAll(0, 0, 0);
    showStrip();
    maximumPeakDifference = 110;
}


//Display one strip
void showOneStrip(boolean left) {
    if(left){
        stripL.show(); 
    }
    else{
        stripR.show();
    }
}


void setColorToOneStripForGivenPixel(int Pixel, byte red, byte green, byte blue, boolean left) {
   if(left){
      stripL.setPixelColor(Pixel, stripL.Color(red, green, blue));
   }
   else{
      stripR.setPixelColor(Pixel, stripR.Color(red, green, blue));
   }
}


void setColorToOneStrip(byte red, byte green, byte blue, boolean left) {
  for(int i = 0; i < NUM_LEDS; i++ ) {
    setColorToOneStripForGivenPixel(i, red, green, blue, left); 
  }
  showOneStrip(left);
}

