/* 
 * 1) After lot of iterations, we found TV wire value is more than 500 for ON condition and OFF is lesser than that.
 * 2) In future, sampling needs to be done for other components.
 */
int baseCurrentSensorValueForOnStatus = 500; //this is for tv- sampled

/**
 * Method returns a fixed value based on the current that goes thru the wire.
 */
int getCurrentSensorData(bool realTime , int noOfSamples) {
      
      //we are ok for a 1 seconds delay for status light - when realtime we get from sensor - this is removed because led was getting stuck whenever this value was fetched.
     if(realTime){
        /*Current Sensor code*/
        float Irms = 0;    // Calculate Irms only
                            //currentSensorActualValue = Irms*230.0;     // Apparent power
        
        for(int i = 0 ; i < noOfSamples ; i ++){
             Irms = Irms + emonMonitor.calcIrms(1480);
        }
        Irms = Irms/noOfSamples;  //One is for the initial value calculation
        
        currentSensorActualValue = Irms * 1000;
        //Serial.print(secondsElapsed ); 
        //Serial.print("--");
        //Serial.println(currentSensorActualValue ); 
     }
     //Serial.println(currentSensorActualValue); 
     return currentSensorActualValue;
}

/**Returns 0 or 1 for returning back to UI based on base value**/
int getONorOFFCurrSensorState(bool realTime , int noOfSamples){
     if(getCurrentSensorData(realTime , noOfSamples) > baseCurrentSensorValueForOnStatus ){
        return 1;
     }
     else{
        return 0;
     }
}



