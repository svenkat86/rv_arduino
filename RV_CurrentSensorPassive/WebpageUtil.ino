//Looks like after the below string pattern - the request line ends
const String stopProcessingString = "HTTP/1.1";

//Currentsensor value param - only for read only purpose
String currentSensorParam = "csen";

void renderServer(){
  //Serial.println("RenderServer Method");
  String HTTP_req;
      EthernetClient client = server.available();
      //long startTime = millis();
      if (client) {
            //Serial.println("New client available");
            String tempBufferString; 
          
            // an http request ends with a blank line
            boolean currentLineIsBlank = true;
            //Stop processing after you get a request with certain characters end. (stopProcessingString)
            boolean stopProcessing = false;
            
            while (client.connected()) {
              
                  if (client.available()) {
                       char c = client.read();
                       //Serial.write(c);
                       //The below statement appends only the first 100 characters - After which it will not append to 'tempBufferString'.
                      
                      //Will use only the tempBufferString after going thru full request- or u can enable the stopProcessing flag.
                      //Stop processing after you get a request with stopProcessingString && the string should have values after lookFor for processing (eg. csen=xyz) here lookfor is 3.
                      //If its favicon - stop processing request immediately.
                      if(tempBufferString.indexOf("favicon.ico") > 0 ||  tempBufferString.indexOf(stopProcessingString) > 0)
                      {
                          stopProcessing = true;
                      }
    
                       //One value after the stopProcessingString will be stored
                      tempBufferString += c; 
    
                      // If you've gotten to the end of the line (received a newline character) and the line is blank (first line), the http request has ended, Or if its stopProcessing equals true
                      // We can stop processing request and send back the Response.
                    
                      if ((c == '\n' && currentLineIsBlank) || stopProcessing) {
                        
                           //requestMadeFromBrowser = true;
                           //Starting on client response 
                           client.println("HTTP/1.1 200 OK");
                           client.println("Server: Arduino");
                           client.println("content-type:application/json; charset=utf-8"); 
                           client.println("Access-Control-Allow-Origin: *");
                           client.println("Connection: close");
                           client.println();
          
                           //Always readonly.
                           client.print(getCurrentStateOfSystemAsJSON());
                           client.println();
                           break;
                      }
          
                      if (c == '\n') {
                          // you're starting a new line
                          currentLineIsBlank = true;
                      }
                      else if (c != '\r') {
                          // you've gotten a character on the current line
                          currentLineIsBlank = false;
                      }
                  }
            }
    
            // give the web browser time to receive the data
            delay(1);
            // close the connection:
            client.stop();
     }
}

String getCurrentStateOfSystemAsJSON(){
      String response = "{";
      String tempJson = "";

      //Current Sensor value - only for read only
      tempJson = createJsonString (currentSensorParam, String(getONorOFFCurrSensorState( true , 2 )));
      response.concat(tempJson);
      
      response.concat("}");
      return response;
}

const char doubleQuote = '"'; 
/**
 * Creates a json formatted String for the passed key and value.
 */
String createJsonString (String jsonKey , String jsonVal) {
    String tempVal = String(doubleQuote);
    tempVal.concat(jsonKey);
    tempVal.concat(doubleQuote);
    tempVal.concat(":");
    tempVal.concat(doubleQuote);
    tempVal.concat(jsonVal);
    tempVal.concat(doubleQuote);
    /* "jsonKey" : "jsonVal" */
    return tempVal;
}









