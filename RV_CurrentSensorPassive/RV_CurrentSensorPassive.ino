// Include Emon Library
#include "EmonLib.h"

// Create an instance
EnergyMonitor emonMonitor;

#define redStatusPin 8    //Connected to Red Led on current sensor - for status
#define greenStatusPin 9  //Connected to Green Led on current sensor - for status

const int currentSensor_Input = A1; //Connected to sensor to get input
volatile int currentSensorStatus = 0;

int currentSensorActualValue = 0;

void setup() {
    pinMode(redStatusPin , OUTPUT);
    pinMode(greenStatusPin , OUTPUT);
    
    // put your setup code here, to run once:
    initializeEthernet();
    emonMonitor.current(currentSensor_Input , 111.1);  // Current: input pin, calibration.
    currentSensorStatus = getONorOFFCurrSensorState(true , 2 );
}

void loop() {
      // put your main code here, to run repeatedly:
      renderServer();
       
      currentSensorStatus = getONorOFFCurrSensorState(true , 1);
  
      if(currentSensorStatus == 1){
          //TV is on
          turnOnGreenLed();
          turnOffRedLed();
      }
      else{
          //TV is off
          turnOffGreenLed();
          turnOnRedLed();
      }
}
