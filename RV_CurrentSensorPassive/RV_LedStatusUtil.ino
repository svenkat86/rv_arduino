void turnOnRedLed() {
    digitalWrite(redStatusPin , HIGH);
}

void turnOnGreenLed() {
    digitalWrite(greenStatusPin  , HIGH);
}

void turnOffRedLed() {
    digitalWrite(redStatusPin , LOW );
}

void turnOffGreenLed() {
    digitalWrite(greenStatusPin , LOW);
}
