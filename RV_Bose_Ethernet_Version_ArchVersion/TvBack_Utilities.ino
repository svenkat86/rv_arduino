/* Controls the tv back led strip */
void setTvBackValuesForLedFully(byte red, byte green, byte blue) {
    updateLedSystemBasedOnPointer(tvBackLedStrip , NUM_LEDS_TVBCK , red, green, blue);
}

void runningLightsTvBack(byte red, byte green, byte blue, int waveDelay){
    runningLightsGeneric(tvBackLedStrip , NUM_LEDS_TVBCK , rColorCode , gColorCode , bColorCode, waveDelay); 
}

void theaterChaseRainbowTvBack(int speedDelay , int loopIdx){
    theaterChaseRainbowGeneric(tvBackLedStrip , NUM_LEDS_TVBCK , speedDelay , loopIdx); 
}
