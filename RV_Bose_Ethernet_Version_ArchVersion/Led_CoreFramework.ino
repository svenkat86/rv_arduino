/* Generic light to turn On and Off based on pointer */
void updateLedSystemBasedOnPointer(Adafruit_NeoPixel &neoPixel , int ledCount , byte red, byte green, byte blue) {
    if(&neoPixel){
       for(int i = 0; i < ledCount ; i++ ) {
          neoPixel.setPixelColor(i , neoPixel.Color(red, green, blue));
       }
       neoPixel.show();
    }
}


uint32_t wheelColorBasedOnStripAndPosition(Adafruit_NeoPixel &neoPixel , byte WheelPos) {
    WheelPos = 255 - WheelPos;
    if(WheelPos < 85) {
        return neoPixel.Color(255 - WheelPos * 3, 0, WheelPos * 3);
    } 
    else if(WheelPos < 170) {
        WheelPos -= 85;
        return neoPixel.Color(0, WheelPos * 3, 255 - WheelPos * 3);
    } 
    else {
        WheelPos -= 170;
        return neoPixel.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
    }
}

