/*Pointer Approach*/
/*
 * Running lights generic framework code
 */
void runningLightsGeneric(Adafruit_NeoPixel &addressPixel , int genericLedCount , byte red, byte green, byte blue, int WaveDelay){
  int Position = 0;
  for(int i=0; i < genericLedCount ; i++)
  {
      Position--;     // = 0; //Position + Rate;
      for(int i=0; i < genericLedCount; i++) 
      {
          // sine wave, 3 offset waves make a rainbow!
          //float level = sin(i+Position) * 127 + 128;
          //setPixel(i,level,0,0);
          //float level = sin(i+Position) * 127 + 128;
          addressPixel.setPixelColor(i, addressPixel.Color(
                                                 ((sin(i+Position) * 127 + 128)/255)*red,
                                                 ((sin(i+Position) * 127 + 128)/255)*green,
                                                 ((sin(i+Position) * 127 + 128)/255)*blue
                                      )
                               );
      }
      addressPixel.show();
      delay(WaveDelay);
  }
}


/*void RunningLights(byte red, byte green, byte blue, int WaveDelay) {
  int Position=0;
  
  for(int i=0; i<NUM_LEDS_TVBCK*2; i++)
  {
      Position--; // = 0; //Position + Rate;
      for(int i=0; i<NUM_LEDS_TVBCK; i++) {
        // sine wave, 3 offset waves make a rainbow!
        //float level = sin(i+Position) * 127 + 128;
        //setPixel(i,level,0,0);
        //float level = sin(i+Position) * 127 + 128;
        setPixel(i,((sin(i+Position) * 127 + 128)/255)*red,
                   ((sin(i+Position) * 127 + 128)/255)*green,
                   ((sin(i+Position) * 127 + 128)/255)*blue);
      }
      
      showStrip();
      delay(WaveDelay);
  }
}
*/








