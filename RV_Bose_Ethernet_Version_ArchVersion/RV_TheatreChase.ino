/* Theatres chases dont have control over colors displayed */
/*
 * genericLedCount - number of leds.
 * addressPixel 
 */
void theaterChaseRainbowGeneric(Adafruit_NeoPixel &addressPixel , int genericLedCount , int speedDelay, int loopIdx){
      byte *c;
      //for (int j=0; j < 256; j++) {     // cycle all 256 colors in the wheel
      for (int q=0; q < loopIdx ; q++) {
          for (int i=0; i < genericLedCount; i=i+loopIdx) {
              addressPixel.setPixelColor(i+q , wheelColorBasedOnStripAndPosition(addressPixel , ( i+ colorMemory) % 255) );
          }
          addressPixel.show();
          delay(speedDelay);
          //Every 3 led - turnOff
          for (int i=0; i < genericLedCount ; i=i+loopIdx) {
               addressPixel.setPixelColor(i+q , addressPixel.Color(0,0,0) );
          }
      }
    
      //How fast you need the color change. - the smaller the number - the faster color change u see. If its higher number its slower.
      if(loopIdx == 1){
          colorMemory = colorMemory + 1;
      }
      else{
          colorMemory = colorMemory + 4;
      }
      
      if(colorMemory >= 256){
          colorMemory = 0;
      }
}
