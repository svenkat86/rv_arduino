//Looks like after the below string pattern - the request line ends
const String stopProcessingString = "HTTP/1.1";

//?aled= --> Arch Led Status : for controlling arch lights
String archLedParam = "aled";

//?tled= --> Tv Led Status : for controlling Tv Back lights
String tvBackLedParam = "tled";

//Color codes - always all 3 should be sent in request.
String rCodeParam = "rcde";    //rCode
String gCodeParam = "gcde";    //gCode
String bCodeParam = "bcde";    //bCode

//MusicPeak Val - from Request - For request send 0 or 1 and for response it returns actual maxpeak value.
String signalFractionParam = "signfrac";

String crazyAlgParam = "crzalg";


void renderServer(){
  //Serial.println("RenderServer Method");
  String HTTP_req;
  EthernetClient client = server.available();
  //long startTime = millis();
  if (client) {
        //Serial.println("New client available");
        String tempBufferString; 
      
        // an http request ends with a blank line
        boolean currentLineIsBlank = true;
        //Stop processing after you get a request with certain characters end. (stopProcessingString)
        boolean stopProcessing = false;
  
        while (client.connected()) {
          
              if (client.available()) {
                   char c = client.read();
                   //Serial.write(c);
                   //The below statement appends only the first 100 characters - After which it will not append to 'tempBufferString'.
                  
                  //Will use only the tempBufferString after going thru full request- or u can enable the stopProcessing flag.
                  //Stop processing after you get a request with stopProcessingString && the string should have values after lookFor for processing.
                  //If its favicon - stop processing request.
                  if(tempBufferString.indexOf("favicon.ico") > 0 ||  tempBufferString.indexOf(stopProcessingString) > 0)
                  {
                      stopProcessing = true;
                  }
                
                   //One value after the stopProcessingString will be stored
                  tempBufferString += c; 
      
                  // If you've gotten to the end of the line (received a newline character) and the line is blank (first line), the http request has ended, Or if its stopProcessing equals true
                  // We can stop processing request and send back the Response.
                
                  if ((c == '\n' && currentLineIsBlank) || stopProcessing) {
                       //requestMadeFromBrowser = true;
                       //Starting on client response 
                       client.println("HTTP/1.1 200 OK");
                       client.println("Server: Arduino");
                       client.println("content-type:application/json; charset=utf-8"); 
                       client.println("Access-Control-Allow-Origin: *");
                       client.println("Connection: close");
                       client.println();
      
                       if(tempBufferString.indexOf("?") >= 0)
                       {         
                            boolean requestContainsArchledParam = tempBufferString.indexOf(archLedParam) > 0 ;
                            
                            boolean requestContainsTvBackLedParam = tempBufferString.indexOf(tvBackLedParam) > 0;
        
                            /* 
                              !!IMPORTANT - This is a else if, which means you cannot pass multiple parameters like on + rdmize + etc
                              Only one at a time it works.
                              http://192.168.0.50?tled=6 --> works
                              http://192.168.0.50?rdm=1 --> works
                              http://192.168.0.50?aled=0&tled=6&rdm=1 ---> Will not work
                              http://192.168.0.50?aled=0 --> Works
                            */    

                            //First process archLed parameters.
                            if(tempBufferString.indexOf(archLedParam) > 0){
                                  String tempArchLedStatusStr = processString(tempBufferString, archLedParam , 2 );  //2- is the param value length
                                  archLedStatus = tempArchLedStatusStr.toInt();
                            }

                            //Next tvBackLed Param
                            if(tempBufferString.indexOf(tvBackLedParam) > 0){
                                  String tvBackLedStatusStr = processString(tempBufferString, tvBackLedParam , 2 );  
                                  tvBackLedStatus = tvBackLedStatusStr.toInt();
                            }

                            if(tempBufferString.indexOf(rCodeParam) > 0  || tempBufferString.indexOf(gCodeParam) > 0 || tempBufferString.indexOf(bCodeParam) > 0 ) 
                            { 
                                  //Parse Color Codes
                                 /******** Red Color Code Processing - Start ********/
                                 String tempCodeVariable = processString(tempBufferString, rCodeParam , 3 );
                                 if(!isValidNumber(tempCodeVariable)) {
                                      //Handle errors
                                      tempCodeVariable = "0";
                                 }
                                 rColorCode = tempCodeVariable.toInt();
                                 /** Red Color Code Processing - End **/
                                 
                                 /** Green Color Code Processing - Start **/
                                 tempCodeVariable = processString(tempBufferString, gCodeParam , 3 );
                                 if(!isValidNumber(tempCodeVariable)) {
                                      tempCodeVariable = "0";
                                 }
                                 gColorCode = tempCodeVariable.toInt();
                                 /** Green Color Code Processing - End **/
        
                                 /** Blue Color Code Processing - Start **/
                                 tempCodeVariable = processString(tempBufferString, bCodeParam , 3 );
                                 if(!isValidNumber(tempCodeVariable) ) {
                                      tempCodeVariable = "0";
                                 }
                                 bColorCode = tempCodeVariable.toInt();
                                 /** Blue Color Code Processing - End **/
                            }  
                           
                            /* Signal Fraction value processing should be one digit and respected only for 0 or 1 */
                            if(tempBufferString.indexOf(signalFractionParam) > 0) {
                                 String signalFractionVal = processString(tempBufferString, signalFractionParam , 1 );
                                 if(signalFractionVal.toInt() == 0){
                                    if(signalFraction > 1){
                                       signalFraction = signalFraction - 0.025;
                                    }
                                 }
                                 else if (signalFractionVal.toInt() == 1){
                                       signalFraction = signalFraction + 0.025;
                                 }
                            }

                            if(tempBufferString.indexOf(crazyAlgParam) > 0) {
                                 String crazyAlgVal = processString(tempBufferString, crazyAlgParam , 1 );
                                 if(crazyAlgVal.toInt() == 1){
                                    crazyAlg = 1;
                                 }
                                 else{
                                    crazyAlg = 0;
                                    resetValuesToInitial();
                                 }
                            }
                            

                            /*else if(tempBufferString.indexOf(randomizeParam) > 0) {
                                  String randomizeStr = processString(tempBufferString, randomizeParam, paramValLengthOne);
                                  randomize = randomizeStr.toInt();
                            }
                            else if(tempBufferString.indexOf(peakValParm) > 0) {
                                 String peakVal = processString(tempBufferString, peakValParm , paramValLengthOne);
                                 if(peakVal.toInt() == 0){
                                    if(maximumPeakDifference > 30){
                                       //lets leave minimum value
                                       maximumPeakDifference = maximumPeakDifference - 10;
                                    }
                                 }
                                 else if (peakVal.toInt() == 1){
                                       maximumPeakDifference = maximumPeakDifference + 10;
                                 }
                            }*/
                            
                        }
                        else {
                            //Default returns the status-Code - when accessing the url directly.
                            
                                
                        }
                        client.print(getCurrentStateOfSystemAsJSON());
                        client.println();
                        break;
                  }
      
                  if (c == '\n') {
                      // you're starting a new line
                      currentLineIsBlank = true;
                  }
                  else if (c != '\r') {
                      // you've gotten a character on the current line
                      currentLineIsBlank = false;
                  }
                  
              }
        }
  
        // give the web browser time to receive the data
        delay(1);
        // close the connection:
        client.stop();
    }
                    
}

/**
 * Method to process and return values from the input string
 * Input string is usually the url with param
 * Looks for valueLength
 * Looks for the string to look for.
 */
String processString(String inputStr, String lookForString, int valueLength){
     //Retain only parameters from ? to blank space " "
     //Below code appends and becomes like 'rcode='
     lookForString.concat("=");
     
     //Calculating the start and end indexes
     int startIdx = inputStr.indexOf(lookForString) + lookForString.length();
     int endIdx = startIdx + valueLength;
     String val = inputStr.substring(startIdx, endIdx);

     //Serial.print("Return value from processString method is ");
     //Serial.println(val);
     return val;
}

String getCurrentStateOfSystemAsJSON(){
      String response = "{";
      String tempJson = "";

      //Arch Led
      tempJson = createJsonString (archLedParam , String(archLedStatus));
      response.concat(tempJson);

      //Tv Back Led
      tempJson = createJsonString (tvBackLedParam , String(tvBackLedStatus));
      response.concat(",");
      response.concat(tempJson);

      //Red Color
      tempJson = createJsonString (rCodeParam , String(rColorCode));
      response.concat(",");
      response.concat(tempJson);

      //Green Color
      tempJson = createJsonString (gCodeParam , String(gColorCode));
      response.concat(",");
      response.concat(tempJson);

      //Blue Color
      tempJson = createJsonString (bCodeParam , String(bColorCode));
      response.concat(",");
      response.concat(tempJson);

      //Signal Fraction value
      tempJson = createJsonString (signalFractionParam, String(signalFraction));
      response.concat(",");
      response.concat(tempJson);

      //Signal Fraction value
      tempJson = createJsonString (crazyAlgParam , String(crazyAlg));
      response.concat(",");
      response.concat(tempJson);
           
      response.concat("}");
      return response;
}


//isNum = isDigit(str.charAt(i)) || str.charAt(i) == '+' || str.charAt(i) == '.' || str.charAt(i) == '-';
//Check if its a number - any digits - no plus or minus sign
boolean isValidNumber(String str){
   boolean isNum = false;
   for(byte i=0; i<str.length(); i++)
   {
       //loop thru each digit to check if its digit - can handle more condition here.    
       isNum = isDigit(str.charAt(i)); 
       if(!isNum)
       {
           //The first occurence of a non-digit character - stop and return false.
           return false;
       }
   }
   return isNum;
}



const char doubleQuote = '"'; 
/**
 * Creates a json formatted String for the passed key and value.
 */
String createJsonString (String jsonKey , String jsonVal) {
    String tempVal = String(doubleQuote);
    tempVal.concat(jsonKey);
    tempVal.concat(doubleQuote);
    tempVal.concat(":");
    tempVal.concat(doubleQuote);
    tempVal.concat(jsonVal);
    tempVal.concat(doubleQuote);
    /* "jsonKey" : "jsonVal" */
    return tempVal;
}



/*Sample Request IMPORTANT*/
/*Arduino Initialized....
New client
GET /?led=1 HTTP/1.1
Host: 192.168.0.16
Connection: keep-alive
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*//*;q=0.8  --> // escape Character - its only one in request
DNT: 1
Accept-Encoding: gzip, deflate, sdch
Accept-Language: en-US,en;q=0.8,ms;q=0.6

Value Substringed ...1
Data Sent In ...571
New client
GET /favicon.ico HTTP/1.1
Host: 192.168.0.16
Connection: keep-alive
Pragma: no-cache
Cache-Control: no-cache
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36
Accept: *//*--> // escape Character - its only one in request
DNT: 1
Referer: http://192.168.0.16/?led=1
Accept-Encoding: gzip, deflate, sdch
Accept-Language: en-US,en;q=0.8,ms;q=0.6
*/


