#include <Adafruit_NeoPixel.h>

/***ALL INPUT PINS*****/
//Input from aux - Left and Right Channel
const int l_audio_Input = A4;
const int r_audio_Input = A5;

/***ALL OUTPUT PINS*****/
//Controlling the LED Strips
#define LedPinArch 2 //the arch 109 leds
#define LedPinTvBck 4 //28 leds back

/*****************************************************/

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
// NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
// NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
// NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
// NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
const int NUM_LEDS_ARCH = 109;
Adafruit_NeoPixel archLedStrip = Adafruit_NeoPixel(NUM_LEDS_ARCH, LedPinArch, NEO_GRB + NEO_KHZ800);
const int NUM_LEDS_TVBCK = 28;
Adafruit_NeoPixel tvBackLedStrip = Adafruit_NeoPixel(NUM_LEDS_TVBCK, LedPinTvBck, NEO_GRB + NEO_KHZ800);

//Attribute to store LED Status
volatile int archLedStatus = 0;
volatile int tvBackLedStatus = 0;

const int sampleWindow = 20;  // Sample window width in mS (50 mS = 20Hz) - used in RV Sync class -from 10 changed to 20
int maximumPeakDifference = 110; 
float signalFraction = 2; //2.3;
int crazyAlg = 0; //For auto-correct logic

//int randomize = 0;
int rColorCode = random(0, 255);
int gColorCode = random(0, 255);
int bColorCode = random(0, 255);

//Using this to flag that a request was recently made
//boolean requestMadeFromBrowser = false;

void resetValuesToInitial(){
    maximumPeakDifference = 110; 
    signalFraction = 2; 
}

void setup() {
    //Serial.begin(9600);
    //Serial.println("Arduino Initialized....");
    
    //Initialize Incoming Mic Input Pins
    pinMode(l_audio_Input, INPUT);
    pinMode(r_audio_Input, INPUT);
    
    //Initialization Of Ethernet Shield
    initializeEthernet();

    archLedStrip.begin();
    archLedStrip.show();
    
    tvBackLedStrip.begin();
    tvBackLedStrip.show();

}

int colorMemory = 0; //in theatrechase
int prevMusicValue = 51;
int times = 0;

void loop() {
        renderServer();
        //First preference is music, so will keep music first, then regular light and finally Off
        if(archLedStatus == 51 || archLedStatus == 52  || archLedStatus == 53 || archLedStatus == 54){
            //During music lets take some time before looping - like spend more time on music
            for(int i=0 ; i < 3 ; i++){
                syncLedToMusic();
            }
        }
        else if(archLedStatus == 55){
            //after 150 times change pattern
            if(times == 150){
              int temp = random(51 , 55);;
              while(prevMusicValue == temp){
                temp = random(51 , 55);
              }
              prevMusicValue = temp;
              times = 0;
            }
            times++;
            archLedStatus = prevMusicValue;
            for(int i=0 ; i < 3 ; i++){
               syncLedToMusic();
            }
            //Set it back to 55
            archLedStatus = 55;
        }
        else if(archLedStatus == 11 ){
              setArchValuesRainbow();
        }
        else if(archLedStatus == 12){
              //Will use last used values and if first time will use the initialization value          
              setArchValuesForLedFully( rColorCode , gColorCode , bColorCode);
        }
        else if(archLedStatus == 13){
              //Led will show only one light at a time - flicker one but fast
              twinkleRandomOnlyOneAtAnyTime(40, 20 );
        }
        else if(archLedStatus == 14){
              //Led will show only one light at a time - flicker one but slow
              twinkleRandomOnlyOneAtAnyTime(1, 500 );
        }
        else if(archLedStatus == 15){
              twinkleRandomMoreThanOne(4, 200);
        }
        else if(archLedStatus == 16){
            theaterChaseRainbowArch(50 , 3);
        }
        else if(archLedStatus == 17){
            theaterChaseRainbowArch(20 , 1);
        }
        else if(archLedStatus == 18){
            runningLightsLedArch(rColorCode , gColorCode , bColorCode, 2); 
        }
        else if(archLedStatus == 0 || archLedStatus == 00) {
             setArchValuesForLedFully(0,0,0);
        }
        

        /**TV Back**/
        if(tvBackLedStatus == 21){
              setTvBackValuesForLedFully( rColorCode , gColorCode , bColorCode);
        }
        else if(tvBackLedStatus == 22){
              runningLightsTvBack(rColorCode , gColorCode , bColorCode, 50); 
        }
        else if(tvBackLedStatus == 23){
              theaterChaseRainbowTvBack(50 , 3);
        }
        else if(tvBackLedStatus == 24){
              theaterChaseRainbowTvBack(30 , 1);
        }
        else if(tvBackLedStatus == 0) {
              setTvBackValuesForLedFully(0 , 0 , 0);
        } 
        
              
}
















