/* Controls the arch led strip */
void setArchValuesForLedFully(byte red, byte green, byte blue) {
    updateLedSystemBasedOnPointer(archLedStrip , NUM_LEDS_ARCH , red, green, blue);
}

/* Controls the arch led strip */
void setArchValuesRainbow() {
    int color =  0;
    for(int i = 0; i < NUM_LEDS_ARCH ; i++ ) {
        color =   map(i, 0, NUM_LEDS_ARCH , 0, 255);
        archLedStrip.setPixelColor(i , wheelColorBasedOnStripAndPosition( archLedStrip , color ) ); 
    }
    archLedStrip.show();
}

/**
 * Random order of turning-on one led at a time.
 * boolean true\false - will make sure only one led is on at any particular instance.
 */

int resetAt = 27;
int tempTwinkleCounter = 0;
void twinkleRandomMoreThanOne(int count, int speedDelay) {
    if(tempTwinkleCounter == 0 || tempTwinkleCounter > resetAt){
        setArchValuesForLedFully( 0 , 0 , 0);
        tempTwinkleCounter = 0;
    }
    tempTwinkleCounter++;
    
    for (int i=0; i < count; i++) {
         int randomLed = random(0, (NUM_LEDS_ARCH-1));
         archLedStrip.setPixelColor(randomLed, random(1,255), random(1,255), random(1,255) );
         archLedStrip.show();
         delay(speedDelay);
    }
    
}

void twinkleRandomOnlyOneAtAnyTime(int count, int speedDelay) {
      for (int i=0; i < count; i++) {
         archLedStrip.setPixelColor( random(0, (NUM_LEDS_ARCH-1)) , random(1,255), random(1,255), random(1,255) );
         archLedStrip.show();
         delay(speedDelay);
         
         //setArchValuesForLedFully(0,0,0); -- Instead of setting 0 for all led everytime, just set for the one we turned it on
         setArchValuesForLedFully( 0 , 0 , 0);
         archLedStrip.show();
      }
}



// int loopIdx -- it is the number of leds movement 
void theaterChaseRainbowArch(int speedDelay , int loopIdx ){
    theaterChaseRainbowGeneric(archLedStrip , NUM_LEDS_ARCH , speedDelay ,  loopIdx); 
}



void runningLightsLedArch(byte red, byte green, byte blue, int waveDelay){
    runningLightsGeneric(archLedStrip , NUM_LEDS_ARCH , rColorCode , gColorCode , bColorCode, waveDelay); 
}
