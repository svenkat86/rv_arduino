#include <SPI.h>
#include <Ethernet.h>

byte mac[] = {
   0x00, 0xAA, 0xBB, 0xCC, 0xDD, 0x50
};

IPAddress ip( 192, 168, 0, 50 );
EthernetServer server(80);
// the dns server ip
IPAddress dnServer(192, 168, 0, 1);
// the router's gateway address:
IPAddress gateway(192, 168, 0, 1);
// the subnet:
IPAddress subnet(255, 255, 255, 0);

//ethernet shield connection initialization
void initializeEthernet()
{
   Ethernet.begin(mac, ip);
   server.begin();
   //Serial.print("server is at ");
   //Serial.println(Ethernet.localIP());
}
