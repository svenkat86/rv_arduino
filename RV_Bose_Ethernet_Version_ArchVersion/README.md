# READ Me for this project

The ip for this project is set to 192.168.0.50 

Mac  0x00, 0xAA, 0xBB, 0xCC, 0xDD, 0x50

## Urls
### Music >  http://192.168.0.50?aled=00 - Turn-Off

>  http://192.168.0.50?aled=51   -  Music Led Pillars 

>  http://192.168.0.50?aled=52   -  Music Led Pillars till up 

>  http://192.168.0.50?aled=53   -  Inverse full pillar 

>  http://192.168.0.50?aled=54   -  White flash 

>  http://192.168.0.50?aled=55   -  Randomize music light 

### Tv Arch Light - Generic
>  http://192.168.0.50?aled=11   -  Arch Rainbow -Simple 

>  http://192.168.0.50?aled=12   -  One color to be set for all leds 

>  http://192.168.0.50?aled=13   -  Twinkle one random - Fast 

>  http://192.168.0.50?aled=14   -  Twinkle one random - Slow 

>  http://192.168.0.50?aled=15   -  Twinkle more than one random 

>  http://192.168.0.50?aled=16   -  Theatre chase 

>  http://192.168.0.50?aled=17   -  Ambience slow fade 

>  http://192.168.0.50?aled=18   -  One color running light (fixed) 

### LED Backlight > http://192.168.0.50?tled=00 - Turn-Off
>  http://192.168.0.50?tled=21   -  TV Led back fixed color

>  http://192.168.0.50?tled=22   -  Running led fixed color

>  http://192.168.0.50?tled=23   -  Running NeoAmbience

>  http://192.168.0.50?tled=24   -  Neoambience

### Setting color in general
>  http://192.168.0.50?rcde=200&bcde=200&gcde=200   -  White color

>  http://192.168.0.50?rcde=200&bcde=000&gcde=200   -  Yellow color

>  http://192.168.0.50/?rcde=000&bcde=000&gcde=150  - Green

### Crazy alg 
>  http://192.168.0.50?crzalg=1

>  http://192.168.0.50?crzalg=0

