int prevPeakVal = 0;

void syncLedToMusic(){
        unsigned long startMillis= millis();  // Start of sample window
        unsigned int peakToPeak = 0;   // peak-to-peak level
        unsigned int signalMax = 0; //0 default
        unsigned int signalMin = 100; 
        unsigned int sampleAvg = 0;

        // collect data for 50 mS
        while ((millis() - startMillis) < sampleWindow)
        { 
              //Got some noise when continously used as Input- so cancel flip\flop
              //pinMode(l_audio_Input, OUTPUT); //changing input to out snd input again has 
              //pinMode(l_audio_Input, INPUT);
              
              int sampleL = analogRead(l_audio_Input);
              int sampleR = analogRead(r_audio_Input); //If both earpiece values are required only
              
              sampleAvg = (sampleL+sampleR)/2;
              if (sampleAvg < 1024)  // toss out spurious readings - changed from 1024 default to 1280
              {
                 if (sampleAvg > signalMax)
                 {
                    signalMax = sampleAvg;  // save just the max levels
                 }
                 else if (sampleAvg < signalMin)
                 {
                    signalMin = sampleAvg;  // save just the min levels
                 }
              }
         }

           peakToPeak = signalMax/signalFraction - signalMin/signalFraction;


           /*Crazy algortihm that worked for alexa - some math - START */
           if(crazyAlg == 1){
               if(signalFraction <= 1){
                  signalFraction = 1;
                }           
                if(peakToPeak > maximumPeakDifference){
                    signalFraction = signalFraction + 0.025;
                    /*Serial.print("SignalMax : ");
                    Serial.print(signalMax);
                    Serial.print(" ");
                    Serial.print("Min ");
                    Serial.print(signalMin);
                    Serial.print(" ");
                    Serial.print("Peak to Peak ");
                    Serial.println(peakToPeak); */
               }

               if(peakToPeak < 7 && signalFraction > 0) {
                    //Serial.print("Peak to peak is : ");
                    //Serial.println(peakToPeak);
                    signalFraction = signalFraction - 0.025;
               }

              if(signalMax == 0 && signalMin == 0 ){
                 //Inbetween songs reset to initial Arduino value or before and after pause
                 //Serial.print("Reseting to initial values.. ");
                 resetValuesToInitial();
              }
           }
           /*Crazy algortihm that worked for alexa - some math -END */  
           
       
          int tempNumOfLedCountToLightUp = 0;
       
          int startingPointOfLeftLight = 0;
          int startingPointOfRightLight = 0;

          int endPointOfLeftLight = 0;
          int endPointOfRightLight = 0;

          //Turning off all Arch Leds First
          setArchValuesForLedFully(0, 0, 0);

          if(archLedStatus == 51 || archLedStatus == 52){ //51 and 52 are pillars small and large
               //Default where both are like pillars - 28 leds
               if(archLedStatus == 51){
                    tempNumOfLedCountToLightUp = 28;
               }
               else {
                    //else if(musicLedStatus == 52){ - avoid a check
                    tempNumOfLedCountToLightUp = 48; //55 is the center Led. so adding 48 with starting point leds will bring to center led , which is 55
               }
    
               startingPointOfLeftLight = 103;
               endPointOfLeftLight = startingPointOfLeftLight - tempNumOfLedCountToLightUp;
    
               startingPointOfRightLight = 7;
               endPointOfRightLight = startingPointOfRightLight + tempNumOfLedCountToLightUp;
               
               int noOfLedsToLightUp = map(peakToPeak, 0, maximumPeakDifference, 0, tempNumOfLedCountToLightUp);
    
               if(noOfLedsToLightUp > tempNumOfLedCountToLightUp){
                   //Usually this doesnt happen, still to stablize and keep value below max led count, randomizing.                
                   noOfLedsToLightUp = random(tempNumOfLedCountToLightUp - 3 , tempNumOfLedCountToLightUp);
               }
    
               for(int i = 0 ; i <  noOfLedsToLightUp ; i++) {
                   int color = map(i, 0, tempNumOfLedCountToLightUp , 0, 255);
                   uint32_t  wheelColorNew = wheelColorBasedOnStripAndPosition(archLedStrip , color);
                   
                   archLedStrip.setPixelColor(startingPointOfLeftLight-1, wheelColorNew ); 
                   startingPointOfLeftLight = startingPointOfLeftLight - 1;
    
                   archLedStrip.setPixelColor(startingPointOfRightLight-1 , wheelColorNew); 
                   startingPointOfRightLight = startingPointOfRightLight + 1;
               }
               /*Turn on LEDs from bottom to up - Left*/
               /*for(int i= startingPointOfLeftLight ; i > (startingPointOfLeftLight-noOfLedsToLightUp)  ; i--)
               {
                   int color = map(i-1, 0, tempNumOfLedCount , 0, 255);
                   stripArch.setPixelColor(i-1, Wheel(color)); 
               }
               
               //Turn on LEDS from bottom to up - Right
               for(int j = startingPointOfRightLight ; j < (startingPointOfRightLight + noOfLedsToLightUp) ; j++)
               {
                   int color = map(j-1, 0, tempNumOfLedCount , 0, 255);
                   stripArch.setPixelColor(j-1, Wheel(color)); 
               }*/
           }
           else if(archLedStatus == 53){
                 tempNumOfLedCountToLightUp = 48;
                 
                 int noOfLedsToLightUp = map(peakToPeak, 0, maximumPeakDifference, 0, tempNumOfLedCountToLightUp);
                 
                 if(noOfLedsToLightUp > tempNumOfLedCountToLightUp){
                     noOfLedsToLightUp = tempNumOfLedCountToLightUp;
                 }
      
                 startingPointOfLeftLight = 55;
                 //endPointOfLeftLight = 103;
                 
                 startingPointOfRightLight = 55;
                 //endPointOfRightLight = 6;
                 
                 for(int i= 0 ; i <  noOfLedsToLightUp ; i++) {
                    int color = map(i, 0, tempNumOfLedCountToLightUp , 0, 255);
                    uint32_t  wheelColorNew = wheelColorBasedOnStripAndPosition(archLedStrip , color);
                    
                    archLedStrip.setPixelColor(startingPointOfLeftLight-1, wheelColorNew); 
                    startingPointOfLeftLight = startingPointOfLeftLight + 1;
      
                    archLedStrip.setPixelColor(startingPointOfRightLight-1, wheelColorNew); 
                    startingPointOfRightLight = startingPointOfRightLight - 1;
                 }
      
                 //Turn on LEDs from center to left -- OLD CODE
                 /*for(int i = tempNumOfLedCount ; i > (tempNumOfLedCount+noOfLedsToLightUp)  ; i++)
                 {
                     int color = map(i-1, 0, tempNumOfLedCount , 0, 255);
                     stripArch.setPixelColor(i-1, Wheel(color)); 
                 }
                 */
                  //Turn on LEDS from center to right -- OLD CODE
                  /*for(int i= NUM_LEDS_ARCH/2 ; i > (tempNumOfLedCount-noOfLedsToLightUp)  ; i--)
                  {
                       int color = map(i-1, 0, tempNumOfLedCount , 0, 255);
                       stripArch.setPixelColor(i-1, Wheel(color)); 
                  }*/
                  
             }
             else if(archLedStatus == 54) {
                 if(peakToPeak == 0){
                     setArchValuesForLedFully(0, 0, 0);
                 }
                 else if (prevPeakVal < peakToPeak){
                     setArchValuesForLedFully(30, 30, 30);
                 }
                 else if (prevPeakVal >  peakToPeak){
                     setArchValuesForLedFully(100, 100 , 100);
                 }
                 prevPeakVal = peakToPeak;
                 delay(50);
             }
                  
             archLedStrip.show();
}


