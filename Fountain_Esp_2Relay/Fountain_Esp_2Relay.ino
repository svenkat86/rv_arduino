#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

const char* ssid = "Paradise_Network";
const char* password = "HowYouDoin5";

ESP8266WebServer server(80);

/*GPIO pins*/
const int pin0 = 0;
const int pin2 = 2;

/*Pin States*/
int pin0_state = 0;
int pin2_state = 0;

long lastMillis = 0;
int tempState = 0;

void handleRoot() {
    server.send(200, "text/json",  "{\"status\":" + String(pin0_state) + "}");
}

void handleNotFound(){
    String message = "File Not Found - RV \n\n";
    message += "URI: ";
    message += server.uri();
    message += "\nMethod: ";
    message += (server.method() == HTTP_GET)?"GET":"POST";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";
    for (uint8_t i=0; i<server.args(); i++){
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    server.send(404, "text/plain", message);
}

void setup(void){
    pinMode(pin0 , OUTPUT);
    pinMode(pin2 , OUTPUT);
    digitalWrite(pin0 , pin0_state);
    digitalWrite(pin2 , pin2_state);

    IPAddress ip(192, 168 , 0 , 61 );    
    IPAddress gateway(192, 168, 0, 1);
    IPAddress subnet(255, 255, 255, 0);
    WiFi.config(ip, gateway, subnet);
    //WiFi.hostname("Esp-Waterfntn"); 
    WiFi.begin(ssid, password);
    
    //Wait for connection
    while (WiFi.status() != WL_CONNECTED) {
       delay(500);
    }
    if (MDNS.begin("esp8266")) {
      Serial.println("MDNS responder started");
    }

    /*Handling root case*/
    server.on("/", handleRoot);

    server.on("/pin0/0", []() {
      lastMillis = millis();
      pin0_state = 0;
      tempState = 0;
      digitalWrite(pin0 , pin0_state );
      server.send(200, "text/json", "{\"status\":" +String(pin0_state)+ "}");
    } );
    
    server.on("/pin0/1", [](){
      lastMillis = millis();
      pin0_state = 1;
      tempState = 1;
      digitalWrite(pin0 , pin0_state );
      server.send(200, "text/json", "{\"status\":" +String(pin0_state)+ "}");
    });

    server.on("/pin2/0", [](){
      pin2_state = 0;
      digitalWrite(pin2 , pin2_state);
      server.send(200, "text/json", "{\"status\":" +String(pin2_state)+ "}");
    });

    server.on("/pin2/1", [](){
      pin2_state = 1;
      digitalWrite(pin2 , pin2_state);
      server.send(200, "text/json", "{\"status\":" +String(pin2_state)+ "}");
    });

    server.onNotFound(handleNotFound);
    server.begin();

}

/***
 * Whenever the pin is turned on from url,
 * we are turning on the motor\pin0 on for 15 minutes and
 * turn off for 5 minutes
 */
void checkAndChangeFountainState()
{
    if(pin0_state == 1){
         int secondsPassed = (millis() - lastMillis)/1000;
         if( secondsPassed > 900 && tempState == 1){
              //Turn OFF after seconds passed and if tempState is ON
              lastMillis = millis();
              tempState = 0;
              digitalWrite(pin0 , 0);
         }
         else if ( secondsPassed > 300 && tempState == 0){
              //Turn ON after seconds passed and if tempState is OFF
              lastMillis = millis();
              tempState = 1;
              digitalWrite(pin0 , 1); 
         }
    }
    else{
        //Switched off case 
    }
}



void loop(void){
    server.handleClient();

    checkAndChangeFountainState();
}



